﻿/*-------------------------------------CREATÝNG COMPANY DATABASE--------------------------------------------------------*/

Create table employee (

emp_id int primary key,
first_name varchar(40),
last_name varchar(40),
birthday date,
sex varchar(1),
salary int,
super_id int,
branch_id int )

Create table branch ( 

branch_id int primary key,
branch_name varchar(50),
mgr_id int,
mgr_start_date date,
foreign key (mgr_id) references employee(emp_id) on delete set null,

Alter table employee 
add foreign key (branch_id)
references branch(branch_id)
on delete set null;

Alter table employee 
add foreign key (super_id)
references employee(emp_id)
on delete set null;

Create table client ( 
client_id int primary key,
client_name varchar(40),
branch_id int,
foreign key (branch_id) references  branch (branch_id) on delete set null ),

Create table works_With(
 emp_id int,
client_id int,
total_sales int,
primary key(emp_id, client_id)
foreign key(emp_id) references employee(emp_id) on delete cascade,
foreign key(client_id) references client(client_id) on delete cascade ) ,


create table branch_supplier (

branch_id int,
supplier_name varchar(50),
supply_type varchar(50),
primary key(branch_id, supplier_name),
foreign key (branch_id) references branch (branch_id) on delete cascade,
/*----------------------------Corporate------------------------------------*/

insert into employee values (100,'David','Wallace', '1967-11-17','M',250000,Null,Null)

insert into branch values (1,'Corporate',100,'2006-02-09')

update employee set branch_id=1 where emp_id=100;

insert into employee values (101,'Jan','Levinson',  '1961-05-01','f',110000,100,1)

/*----------------------------Scranton------------------------------------*/

insert into employee values (102,'Michael','Scott', '1964-03-15','M',750000,null,null)
insert into branch values (2,'Scranton',102,'1992-04-06')
update employee set branch_id = 2 where emp_id =102;


insert into employee values (103,'Angela','Martin', '1971-06-25','M',63000,102,2)
insert into employee values (104,'Kelly','Kapoor', '1980-02-05','F',55000,102,2)
insert into employee values (105,'Stanley','Hudson', '1958-02-19','M',69000,102,2)


/*----------------------------Stamford------------------------------------*/
insert into employee values (106,'Josh','Porter','1969-09-05','M',78000,Null,Null)
insert  into branch values (3,'Stamford',106,'1998-02-13')

update employee set branch_id = 3 where emp_id=106;

insert into employee values (107,'Andy','Bernard','1973-07-22','M',65000,106,3)
insert into employee values (108,'Jim','Halpert','1978-10-01','M',71000,106,3)


/*----------------------------Client------------------------------------*/

insert into client values (400,'Dunmore Highschool',2);
insert into client values (401,'Lackawana Country',2);
insert into client values (402,'FedEx',3);
insert into client values (403,'John Daly Law , LLC',3);
insert into client values (404,'Scranton Whitepages',2);
insert into client values (405,'Times Newspaper',3);
insert into client values (406,'FedEx',2);



/*----------------------------Works_With------------------------------------*/

insert into works_With values (105,400,55000)
insert into works_With values (102,401,267000)
insert into works_With values (108,402,22500)
insert into works_With values (107,403,5000)
insert into works_With values (108,403,12000)
insert into works_With values (105,404,33000)
insert into works_With values (107,405,26000)
insert into works_With values (102,406,15000)
insert into works_With values (105,406,130000)


/*----------------------------More Basic Queries------------------------------------*/

/*----Find all employees-----*/
select * from employee
/*-----find all clients-------*/
select * from client
/*-----find all employees order by salary------------*/
select * from employee order by salary desc

/*-----find all employees order by sex and name------------*/
select * from employee order by sex,first_name

/*-----find the first and last names of all employees------------*/
select first_name,last_name from employee

/*-----find the forename and surnames of all employees------------*/
select first_name as forename ,last_name as surname from employee


/*-----find out all  the different genders------------*/

select distinct branch_id from employee;

/*-----------------------------------Functions-----------------------*/

/*-----Find the number of employees------------*/
select count(super_id) from employee;

/*-----find the number of female employees born after 1970------------*/
select count(emp_id) from employee where sex ='F' and birthday > '1970'

/*-----find the average of all employees salaries------------*/
select avg(salary) from employee;

/*-----find the sum of all employees salaries------------*/
select sum(salary) from employee


/*-----find out how many males and females are there------------*/

select count(sex),sex  from employee group by sex


/*-----find the total sales of each salesman------------*/

select sum(total_sales), emp_id from works_With group by emp_id


/*---------------------------Wildcards---------------------------*/

/*-----find all the client's who are an LLC------------*/

select * from client where client_name like '%LLC%'

/*-----find all the client's who are in Label Business------------*/

select * from branch_supplier where supplier_name like '%Label%';


/*-----find any employee born in October ------------*/

select * from employee where birthday like '%__-02%'





/*------- ----------------------------------Union---------------------------------*/

/*--------Find a list of employee and brach names-------*/

select first_name  from employee union select branch_name from branch 
union select client_name from client;

/*--------Find a list of all clients and branch suppliers names---------*/

select client_name, client.branch_id from client union select supplier_name,branch_id from branch_supplier

/*---------Find a lisft of all money spent or earned by the company-----------*/

select salary from employee union select total_sales from works_With;



/*------------------------------------------Joints---------------------------------*/

insert into branch values (4,'Buffalo',Null,Null)



/*------Find all branches and the names of their managers----------*/

select employee.emp_id, employee.first_name, branch.branch_name from employee
join branch on employee.emp_id=branch.mgr_id

select employee.emp_id, employee.first_name, branch.branch_name from employee
left join branch on employee.emp_id=branch.mgr_id

select employee.emp_id, employee.first_name, branch.branch_name from employee
right join branch on employee.emp_id=branch.mgr_id



/*------- ----------------------------------Nestes Queries---------------------------------*/

/*-------Find names of all employees who have sold over 30000 to a single client---------*/

select employee.first_name, employee.last_name from employee
where employee.emp_id IN (select works_With.emp_id from works_With where works_With.total_sales > 30000)


/*-------Find all clients who are handled by the branch that michael scott manages. Assume you know Michael's ID---------*/

select client.client_name from client
where client.branch_id IN (select branch.branch_id from branch where branch.mgr_id > 102)