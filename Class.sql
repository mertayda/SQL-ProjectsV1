﻿/*------CREATING AND DROPING DATABASE---------*/


drop table student;


create table student (

student_id int primary key,
name varchar(50) ,
major varchar(20)  );
select * from student;


create table student (

student_id int primary key,
name varchar(50)  not null,
major varchar(20)  Unique );

create table student (

student_id int primary key,
name varchar(50)  ,
major varchar(20)  default 'Undecided' );


insert into  student values  (1, 'Jack','Biology');
insert into student values (2, 'Kate', 'Sociology');
insert into student values (3, 'Claire', 'Chemistry');
insert into student values (4,'Jack' , 'Biology');
insert into student values (5, 'Mike', 'Computer Science');


insert into  student values  (1, 'Jack','Biology');
insert into student values (2, 'Kate', 'Sociology');
insert into student(student_id,name) values (3, 'Claire');
insert into student values (4,'Jack' , 'Biology');
insert into student values (5, 'Mike', 'Computer Science');
select * from student;


/*-------UPDATING AND DELETING DATABASE------*/
select * from student
update student set major='bio' where major ='Biology';
update student set major='Comp Sci' where major ='Computer Science';
update student set major= 'Biochemistry' where major ='bio' or  major='Chemistry'
update student set name= 'Tom', major= 'undecided' where student_id=1;
update student  set major ='undecided';

delete from student ;

delete from student where student_id=5;
delete from student where name ='tom' and major='undecided';


/*--------------------------BASÝC QUERY----------------------*/

select * from student;
select name from student;
select name,major from student;
select student.name, student.major from student order by student_id asc;
select * from student order by student_id desc;
select * from student order by major, student_id asc;
select  top 2 * from student;
select top 2 * from student order by student_id asc;
select *  from student where major= 'Chemistry';
select name, major from student where major='Chemistry' or major='Biology';
select name,major from student where major <> 'Chemistry';
select * from student where student_id < 3;
select * from student where student_id < 3 and name <> 'Jack';
select * from student where name in ('mike', 'jack' ,'Claire');
select * from student where major in ('Biology', 'Chemistry') and  student_id > 

/*-------------------------------------CREATÝNG COMPANY DATABASE--------------------------------------------------------*/

Create table employee (

emp_id int primary key,
first_name varchar(40),
last_name varchar(40),
birthday date,
sex varchar(1),
salary int,
super_id int,
branch_id int )